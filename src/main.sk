#
#     Locust, a Lemmy web client
#
#     Copyright (C) 2023 gitlab.com/brlf (brie@beehaw.org)
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

@entry
def main {
	dynamic.window.addEventListener("hashchange", (_) => route(dynamic.window.location.hash.slice(1)))
	route(dynamic.window.location.hash.slice(1))
}

const posts_per_page = 30

class Route {
	def new(e string, h dynamic) {
		expression = RegExp.new("^"+e+"$")
		handler = h
	}
	var expression RegExp
	var handler dynamic # Function to handle calls

	def handle(path string) Promise<dynamic> {
		var match = expression.exec(path)
		if match == null { return null }

		match[0] = match.input
		return handler.apply(null, match)
	}
}

const routes = [
	Route.new("/([^/?]+)(\\?.*)?", (path string, instance string, query string) => {
		var page = 1
		var search URLSearchParams
		if query != null {
			search = URLSearchParams.new(query)
			page = dynamic.parseInt(search.get("page", "1"))
			if (page as double).isNaN {
				page = 1
			}
		} else {
			search = URLSearchParams.new
		}
		search.set("page", (page + 1).toString)

		var api = LemmyAPI.new("https://\(instance)")

		return api.get_posts(page).then<dynamic>((r) => <UI viewer_instance=instance />.render(r.posts, "#/\(instance)?\(search)", 1 + (page - 1) * posts_per_page))
	}),

	Route.new("/([^/]+)/c/([^/?]+)(\\?.*)?", (path string, instance string, community string, query string) => {
		var page = 1
		var search URLSearchParams
		if query != null {
			search = URLSearchParams.new(query)
			page = dynamic.parseInt(search.get("page", "1"))
			if (page as double).isNaN {
				page = 1
			}
		} else {
			search = URLSearchParams.new
		}
		search.set("page", (page + 1).toString)

		var api = LemmyAPI.new("https://\(instance)")
		return api.get_posts(community, posts_per_page, page).then<dynamic>((r) => <UI viewer_instance=instance />.render(r.posts, "#/\(instance)/c/\(community)?page=\(page + 1)", 1 + (page - 1) * posts_per_page))
	}),

	Route.new("/([^/]+)/post/([^/]+)", (path string, instance string, id string) => {
		var api = LemmyAPI.new("https://\(instance)")

		var post PostView
		var comments List<CommentView>

		return Promise<dynamic>.all([
			api.get_post(dynamic.parseInt(id)).then<dynamic>((r) => post = r.post_view),
			api.get_comments(dynamic.parseInt(id)).then<dynamic>((r) => comments = r.comments),
		]).then<dynamic>(_ => {
			var result = DocumentFragment.new
			var ui = <UI viewer_instance=instance />
			result.append(ui.render(post))
			result.append(ui.render_comments(comments))
			return result
		})
	}),

	Route.new("/([^/]+)/u/([^/]+)", (path string, instance string, user string) => {
		var api = LemmyAPI.new("https://\(instance)")
		return api.get_user(user).then<dynamic>(r => <UI viewer_instance=instance />.render(r.posts))
	}),


	Route.new(".*", () => {
		return Promise<dynamic>.new((r) => r("Not found"))
	}),
]

var active Object
def route(path string) {
	var me = active = Object.new
	var content = dynamic.content_zone
	content.innerHTML = ""
	content.append(document.createElement("progress"))

	if !RELEASE { console.log(path) }

	for route in routes {
		var p = route.handle(path)
		if p == null { continue }

		p.then<dynamic>((e) => {
			if !Object.Is(me, active) { return null }
			content.innerHTML = ""
			content.append(e)
			return null
		}).Catch(()=>{
			if !Object.Is(me, active) { return }
			content.innerHTML = "The page is dead. Try refreshing."
		})
		break
	}
}

class UI {
	def new {}

	var viewer_instance string

	def clone_template(t dynamic) dynamic {
		return t.content.firstElementChild.cloneNode(true)
	}

	def render_markdown(s string) dynamic {
		if s == null { return DocumentFragment.new }
		var c = dynamic.commonmark
		var temp = document.createElement("template")
		temp.innerHTML = c.HtmlRenderer.new({"safe": true }).render(c.Parser().parse(s))
		return temp.content
	}

	def render(p PostView) dynamic {
		var result = clone_template(dynamic.post_template)

		var title = result.querySelector(".post-title")
		title.textContent = p.post.name
		title.href = "#/\(viewer_instance)/post/\(p.post.id)"
		result.querySelector(".post-body").append(render_markdown(p.post.body))
		return result
	}

	def render_feed_post(p PostView) dynamic {
		var result = clone_template(dynamic.feed_post_template)

		var title = result.querySelector(".post-title")
		title.textContent = p.post.name
		title.href = "#/\(viewer_instance)/post/\(p.post.id)"

		result.querySelector(".post-score").textContent = p.counts.score.toString
		result.querySelector(".comment-count").textContent = p.counts.comments.toString

		var link = result.querySelector(".post-link")
		if p.post.url == null {
			link.remove()
		} else {
			link.textContent = "(\(URL.new(p.post.url).host))"
			link.href = link.title = p.post.url
		}

		result.querySelector(".creator-p").replaceWith(render_username(p.creator))

		return result
	}

	def render_username(p Person) dynamic {
		var result = clone_template(dynamic.username_template)
		var actor = URL.new(p.actor_id)
		var user_name = actor.pathname.slice(actor.pathname.lastIndexOf("/")+1)

		result.textContent = p.name
		result.href = "#/\(viewer_instance)/u/\(user_name)@\(actor.host)"

		return result
	}

	def render(l List<PostView>) dynamic {
		var result = document.createElement("div")
		for p in l {
			result.append(render_feed_post(p))
		}
		return result
	}

	def render(l List<PostView>, next_page string, start int) dynamic {
		var result = DocumentFragment.new

		var post_list = document.createElement("ol")
		post_list.setAttribute("start", start.toString)
		for p in l {
			post_list.append(render_feed_post(p))
		}
		result.append(post_list)


		var next = document.createElement("a")
		next.href = next_page
		next.textContent = "More"
		result.append(next)

		return result
	}

	def render_comments_rec(d DocumentFragment, children Map<string, List<CommentView>>, path string, depth int) {
		for child in children.get(path) ?? [] {
			var el = render_comment(child)
			el.style["margin-left"] = "\(depth)rem"
			d.append(el)
			render_comments_rec(d, children, child.comment.path, depth + 1)
		}
	}

	def render_comments(l List<CommentView>) dynamic {
		if !RELEASE { console.log(l) }
		var d = DocumentFragment.new
		var children = Map<string, List<CommentView>>.new

		for comment in l {
			var parent = comment.comment.path.slice(0, comment.comment.path.lastIndexOf("."))
			if !children.has(parent) {
				children.set(parent, [])
			}

			children.get(parent).append(comment)
		}

		render_comments_rec(d, children, "0", 0)

		return d
	}

	def render_comment(c CommentView) dynamic {
		var result = clone_template(dynamic.comment_template)

		result.querySelector(".creator-p").replaceWith(render_username(c.creator))
		result.querySelector(".comment-body").append(render_markdown(c.comment.content))

		return result
	}
}
