# Usage (Development)

You will need to install [skewc](https://github.com/evanw/skew).

Run the following command to build.

```
skewc ./ --output-file=dist/main.js
```

You will also need to download a copy of commonmark.js, such as from
[unpkg](https://unpkg.com/commonmark@0.30.0/dist/commonmark.js). Place it in
dist.
